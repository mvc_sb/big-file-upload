package com.cugb.bigfileupload.servie;

import com.cugb.bigfileupload.bean.FilePO;

import java.util.List;

public interface FileService {
    Integer addFile(FilePO filePO);

    Boolean selectFileByMd5(String md5);

    List<FilePO> selectFileList();
}
