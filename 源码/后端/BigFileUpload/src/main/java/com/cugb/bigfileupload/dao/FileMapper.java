package com.cugb.bigfileupload.dao;

import com.cugb.bigfileupload.bean.FilePO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface FileMapper {
    public Integer insertFile(FilePO filePO) ;

    FilePO selectFileByMd5(String md5);

    List<FilePO> selectFileList();
}
